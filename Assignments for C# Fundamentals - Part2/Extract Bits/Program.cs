﻿using System;
using System.Text.RegularExpressions;

namespace ExtractBits
{
    public class ExtractBits{

        // Static method to identify whether the input is integer or not by using regular expressions
        static bool isNumber(string input){

            // String pattern to verify the integer number
            string pattern = "[0-9]{0,}";

            // Creating new regular expression object
            Regex re = new Regex(pattern);

            //Checking whether the input is integer or not
            if(re.IsMatch(input)){

                // Returning true if it is integer
                return true;
            }
            return false;
        }

        // Static method to get input from the user
        static int GetInput(){

            // Reading input from the console
            string input = Console.ReadLine();

            // loop to check the input string is integer or not
            while(!isNumber(input) ){

                // Displaying message to user to enter the  correctly
                Console.WriteLine("The input should be a positive number please re-enter correctly");
                input = Console.ReadLine();
            }

            // Returning converted input number
            return Convert.ToInt32(input);
        }
        public static void Main(string[] args){

            // Defining variables
            int number, startBit,stopBit;
            string binary;
            Console.WriteLine("Enter the number");
            number = GetInput();   // Calling get input method inorder to take input from the user
            binary = Convert.ToString(number,2);   // Converting to binary
            Console.WriteLine($"The binary form of {number} is {binary}"); // Displaying the number and its bainry form

            // finding the of the binary
            int length = binary.Length;

            Console.WriteLine("Enter the starbit");
            startBit = GetInput();

            // Checking whether the start bit is in correct range or not
            while(startBit>=length-1 ||startBit<0 ){
                Console.WriteLine($"The startBit should be in the range of {0} to {length-2}");
                startBit = GetInput();
            }
            Console.WriteLine("Enter the stopbit");
            stopBit = GetInput();

            // Checking whether the stop bit is in correct range or not
            while(stopBit<=startBit || stopBit>=length){
                Console.WriteLine($"The stopBit should be in the range of {startBit+1} to {length-1}");
                stopBit = GetInput();
            }

            // Creating new string for the sub binary
            string subBinary = "";
            for(int i=length-startBit-1;i>=length-stopBit-1;i--){
                subBinary = binary[i] + subBinary;
            }

            // Displaying the sub binary in its binary form and its decimal form
            Console.WriteLine($"Binary Field Value = {subBinary}");
            Console.WriteLine($"Decimal Field Value = {Convert.ToInt32(subBinary,2)}");
        }
    }
}