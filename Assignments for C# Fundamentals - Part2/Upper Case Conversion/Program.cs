﻿using System;
namespace UpperCaseConversion
{
    public class UpperCaseConversion{
        public static void Main(string[] args){

            // The loop will continue till the user wants to stop
            while(true){

                // Displaying information to the user
                Console.WriteLine("Enter a string");

                // Reading the input string from the user
                string userString = Console.ReadLine();

                //Checking whether the entered string is null of not
                // If it is null then ask user to enter new string
                while(userString==""){
                    Console.WriteLine("Enter the string correctly");
                    userString = Console.ReadLine();
                }

                // Checking whether user wants to stop or not
                if(userString.ToLower()=="stop"){
                    break;
                }

                // Displaying the entered string in Upper Case form
                Console.WriteLine($"Case converted string is {userString.ToUpper()}");

            }
        }
    }
}