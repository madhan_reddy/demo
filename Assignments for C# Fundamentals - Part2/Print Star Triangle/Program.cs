﻿using System;
using System.Text.RegularExpressions;

namespace PrintStarTriangle
{
    public class PrintStarTriangle{
        static void Main(string[] args){
            Console.WriteLine("Enter the Number of rows");

            failSafe:;    // Created a label inorder to start execution from this line
            string input = Console.ReadLine();
            string pattern = "^[0-9]{1,}$";    // Pattern for integer numbers
            Regex re = new Regex(pattern);

            // loop will until the user enters integer number
            while(!(re.IsMatch(input)) ){
                Console.WriteLine("Please enter only a number");
                input = Console.ReadLine();
            }            
            int number = Convert.ToInt32(input);
            if(number>0 && number<=10){   // Checking whether the number is in given range or not
                for(int i=1;i<=number;i++){
                    for(int j=0;j<number-i;j++){
                    Console.Write(" ");
                    }
                    for(int j=0;j<i;j++){
                        Console.Write("* ");
                    }
                    Console.WriteLine();
                }
            }
            else{
                Console.WriteLine("Entered value is not in the given range(0-10) please enter correctly");
                goto failSafe;
            }
        }
    }
}