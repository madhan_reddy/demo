﻿using System;
using System.Text.RegularExpressions;

namespace PerimeterAndArea
{
    
    public class PerimeterAndArea{

        // Static method to find the given number is decimal or not
        static bool isNumber(string input){
            string pattern = "[0-9]{0,}.([0-9]{1,})*";
            Regex re = new Regex(pattern);
            if(re.IsMatch(input)){
                if(input == "0" || input[0]=='-')
                return false;
                else
                return true;
            }
            return false;
        }

        // Static method to get correct from  the user
        static double GetInput(){
            string input = Console.ReadLine();
            while(!isNumber(input) ){
                Console.WriteLine("The given input is wrong please re-enter correctly");
                input = Console.ReadLine();
            }
            return Convert.ToDouble(input);  // Return the converted string to double of given input
        }

        // static method for square
        public static void Square(){
            Console.WriteLine("Enter the side of the square");       

            // Calling GetInput method to get correct input from the user     
            // Getting requrired parameters from the user to find area and perimeter
            double side=GetInput();
            double perimeter = 4*side;  // Calculating perimeter
            double area = side*side;    // Calculating Area

            // Printing area and perimeter
            Console.WriteLine(@$"
            Perimeter = {Math.Round(perimeter,2)}
            Area      = {Math.Round(area,2)}");
        }

        // Static method for rectangle
        public static void Rectangle(){

            // Calling GetInput method to get correct input from the user     
            // Getting requrired parameters from the user to find area and perimeter
            Console.WriteLine("Enter the length of rectangle");
            double length = GetInput();
            Console.WriteLine("Enter the breadth of rectangle");
            double breadth = GetInput();


            double perimeter = 2*(length + breadth);   // Calculating perimeter
            double area = length * breadth;            // Calculating area

            // Printing to the console
            Console.WriteLine(@$"
            Perimeter = {Math.Round(perimeter,2)}
            Area      = {Math.Round(area,2)}");
        }

        //  Static method for circle
        public static void Circle(){

            // Calling GetInput method to get correct input from the user     
            // Getting requrired parameters from the user to find area and perimeter
            Console.WriteLine("Enter the radius of a circle");
            double radius = GetInput();

            // Calculating area and perimeter
            double perimeter = 2*Math.PI*radius;
            double area = Math.PI*radius*radius;

            // Printing to the console
            Console.WriteLine(@$"
            Perimeter = {Math.Round(perimeter,2)}
            Area      = {Math.Round(area,2)}");
        }

        // Static method for triangle
        public static void Triangle(){

            // Calling GetInput method to get correct input from the user     
            // Getting requrired parameters from the user to find area and perimeter
            Console.WriteLine("Enter the first side length of the triangle");
            double length1 = GetInput();
            Console.WriteLine("Enter the second side length of the triangle");
            double length2 = GetInput();
            Console.WriteLine("Enter the third side length of the triangle");
            double length3 = GetInput();

            // Calculating area and perimeter
            double perimeter = length1+length2+length3;
            double area = Math.Sqrt(perimeter*(perimeter-length1)*(perimeter-length2)*(perimeter-length3))/(2*1.0);

            // Printing to the console
            Console.WriteLine(@$"
            Perimeter = {Math.Round(perimeter,2)}
            Area      = {Math.Round(area,2)}");
        }
        public static void Main(string[] args){
            Console.WriteLine("Choose the options(1,2,3,4) given below to find it\'s perimeter and area");

            // Displaying optins to the user
            Console.WriteLine(@"
            1. Square
            2. Rectangle
            3. Circle
            4. Triangle");

            failSafe:;
            int option=Convert.ToInt32(Console.ReadLine());    // Reading the input
            switch(option)
            {

                // Based on the given input its respective method is called by using switch case method
                case 1: 
                    PerimeterAndArea.Square();
                    break;
                case 2: 
                    PerimeterAndArea.Rectangle();
                    break;
                case 3: 
                    PerimeterAndArea.Circle();
                    break;
                case 4: 
                    PerimeterAndArea.Triangle();
                    break;
                default:
                    Console.WriteLine("Please choose the option(1,2,3,4) correctly");
                    goto failSafe;   // Making the program to go to line 117 by using "failSafe" label
                    // break;
            }
            
        }
    }

}