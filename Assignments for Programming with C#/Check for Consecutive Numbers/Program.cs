﻿using System;
using System.Text.RegularExpressions;

namespace CheckForConsecutiveNumbers
{
    public class CheckForConsecutiveNumbers
    {
        static void Main(string[] args){
            Console.WriteLine("Enter the numbers in the pattern as \"1-2-3-...and so on\"");
            string input;
            while(true){
                input = Console.ReadLine();                                         // Taking input from user in the form of string
                string pattern = "^[0-9]{1,}(-[0-9]{1,})*$";
                Regex re = new Regex(pattern);
                if(re.IsMatch(input)){
                    break;
                
                }
                else{
                    Console.WriteLine("The entered pattern is wrong please re-enter once again in the specified pattern");
                }
            }
            string[] inputArray = input.Split('-');                                    // converting the input string into string array by spliting the input string at '-'
            int[] numbersArray = Array.ConvertAll(inputArray,s => int.Parse(s));       // Converting the string array into int array by using ConvertAll array built in method
            bool isConsequtive = true;                                                 // Using a variable to check the status of the array
            for(int i=0;i<numbersArray.Length-1;i++){
                if(Math.Abs(numbersArray[i]-numbersArray[i+1])!=1){
                    isConsequtive=false;
                    break;
                }
            }
            Console.WriteLine("The numbers are {0} consecutive order",isConsequtive ? "in" : "not in");
        }
    }
}